import React, {Component} from 'react';
import axios from 'axios';

const ImagesContext = React.createContext();

class ImagesProvider extends Component {
    state = {
        images: [],
        favorites: [],
        liked: false
    };

    componentDidMount() {
        this.getImages();
    }

    getImages = () => {
        axios.get('https://cors-anywhere.herokuapp.com/http://shibe.online/api/shibes?count=16')
            .then(res => {
                let tempImg = [];
                res.data.forEach(() => {
                    const singleImg = [...res.data];
                    tempImg = singleImg;
                });
                this.setState({
                    images: tempImg
                })
            })
            .catch(err => {
                console.log(err, 'Error from getting data');
            })
    };

    fetchImages = () => {
        axios.get('https://cors-anywhere.herokuapp.com/http://shibe.online/api/shibes?count=16')
            .then(res => {
                this.setState(() => {
                    return {
                        images: [...this.state.images, ...res.data]
                    }
                });
            })
    };

    findInLikedImg = img => {
        const image = this.state.favorites.find(item => item === img);
        return image;
    };

    like = item => {
        this.setState(() => {
            return {
                favorites: [...this.state.favorites, item],
                liked: item
            }
        }, () => {
        });
    };

    dislike = img => {

        let tempFavorites = [...this.state.favorites];

        tempFavorites = tempFavorites.filter(item => item !== img);

        this.setState(() => {
            return {
                favorites: [...tempFavorites]
            }
        })

    };

    clearFavorites = () => {
        this.setState(() => {
            return {
                favorites: []
            }
        })
    };

    render() {
        return (
            <ImagesContext.Provider value={{
                ...this.state,
                fetchImages: this.fetchImages,
                like: this.like,
                dislike: this.dislike,
                findInLikedImg: this.findInLikedImg,
                clearFavorites: this.clearFavorites
            }}>
                {this.props.children}
            </ImagesContext.Provider>
        )
    }
}

const ImagesConsumer = ImagesContext.Consumer;

export {ImagesConsumer, ImagesProvider};