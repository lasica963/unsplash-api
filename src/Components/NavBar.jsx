import {Link} from "react-router-dom";
import React from "react";
import {ImagesConsumer} from "../Context/image.context";


const Navbar = () => (
    <ImagesConsumer>
        {
            value => {
                return (
                    <div className='fix top0 left0 padt20 padb20 padl20 bg--white row z100'>
                        <Link to='/' className='mr20'>
                            Home
                        </Link>
                        <Link to='/favorites'>
                            <span className='undl'>Polubione</span>: {value.favorites.length}
                        </Link>
                    </div>
                )
            }
        }

    </ImagesConsumer>
);
export default Navbar;
