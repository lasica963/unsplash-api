import React from 'react'
import SingleImg from './SingleImg'

export default function ListOfImages({value}) {

    return (
        <React.Fragment>
            <div className="row flex flex-wrap mt60">
                {
                    value.map(item => {
                        return <SingleImg key={value.indexOf(item)} item={item}/>
                    })
                }
            </div>
        </React.Fragment>
    )
}