import React from 'react'
import {ImagesConsumer} from "../Context/image.context";
import loader from '../img/loader.gif';
import InfiniteScroll from 'react-infinite-scroll-component';
import ListOfImages from "./ListsOfImages";


const imageList = () => (
    <div className="App">
        <ImagesConsumer>
            {
                value => {
                    return (
                        <div>

                            <InfiniteScroll
                                dataLength={value.images.length}
                                next={value.fetchImages}
                                hasMore={true}
                                loader={<img src={loader} alt="" className='auto'/>}>
                                {
                                    <ListOfImages value={value.images}/>
                                }
                            </InfiniteScroll>
                        </div>
                    )
                }
            }
        </ImagesConsumer>
    </div>

);
export default imageList;
