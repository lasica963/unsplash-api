import React from 'react';
import {ImagesConsumer} from "../Context/image.context";
import like from "../img/heart-full.png";
import dislike from "../img/heart-empty.png";

export default function SingleImg({item}) {
    return (
        <React.Fragment>
            <ImagesConsumer>
                {
                    value => {
                        return (

                            <div className='single-img col-3 xs-col-6 rel'>
                                <img src={item} alt="" className='row h100 mh100'/>
                                <span className='abs bottom20 right20'>
                                    {
                                        value.findInLikedImg(item) === item ?
                                            <img src={like} alt="" className='btn--like'
                                                 onClick={
                                                     () => {
                                                         value.dislike(item)
                                                     }
                                                 }
                                            /> :
                                            <img src={dislike} alt="" className='btn--like'
                                                 onClick={
                                                     () => {
                                                         value.like(item)
                                                     }
                                                 }
                                            />
                                    }
                                </span>
                            </div>

                        )
                    }
                }
            </ImagesConsumer>
        </React.Fragment>
    )
};

