import React from 'react'
import {ImagesConsumer} from "../Context/image.context";
import like from '../img/heart-full.png';
import dislike from '../img/heart-empty.png';
import trash from '../img/trash.png';

const likedImg = () => (
    <div className="App">
        <ImagesConsumer>
            {
                value => {
                    return (
                        <div className='row flex flex-wrap mt60'>
                            {
                                value.favorites.map((img) => {
                                    return value.findInLikedImg(img) === img ?
                                        <div className='rel col-3 xs-col-6 single-img'>
                                            <img key={value.favorites.indexOf(img)} src={img} alt=""
                                                 className='row h100 mh100'/>
                                            <span className='abs bottom15 right15'>
                                                 <img src={like} alt="" className='btn--like'
                                                      onClick={() => {
                                                          value.dislike(img);
                                                      }}/>
                                            </span>
                                        </div>
                                        :
                                        <div className='rel col-3 xs-col-6 single-img'>
                                            <img key={value.favorites.indexOf(img)} src={img} alt=""
                                                 className='row h100 mh100'/>
                                            <span className='abs bottom15 right15'>
                                                  <img src={dislike} alt="" className='btn--like'
                                                       onClick={() => {
                                                           value.like(img);
                                                       }}/>
                                            </span>
                                        </div>
                                })
                            }
                            {
                                <div className='btn--trash' onClick={value.clearFavorites}>
                                    <img src={trash} alt=""/>
                                </div>
                            }
                        </div>
                    )
                }
            }
        </ImagesConsumer>
    </div>

);
export default likedImg;