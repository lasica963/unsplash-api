import React from 'react';
import {Switch, Route} from 'react-router-dom';
import './sass/core.scss';
import imageList from './Components/imageList.jsx'
import likedImg from './Components/likedImg.jsx';
import NavBar from './Components/NavBar'

const App = () => (
    <div className="App">
        <React.Fragment>
            <NavBar/>
            <Switch>
                <Route exact path='/' component={imageList}/>
                <Route path='/favorites' component={likedImg}/>
            </Switch>
        </React.Fragment>

    </div>
);


export default () => (
    <App/>
)
